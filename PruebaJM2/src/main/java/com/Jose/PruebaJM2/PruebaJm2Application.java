package com.Jose.PruebaJM2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaJm2Application {

	public static void main(String[] args) {
		SpringApplication.run(PruebaJm2Application.class, args);
	}

}
